export interface WorkItem {
    id: string;
    title: string;
    description: string;
    status: string;
    assignee: string;
    created: Date;
    review?;
}