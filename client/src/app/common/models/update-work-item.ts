export interface UpdateWorkItem {
    title?: string;
    description?: string;
    review?: string[];
    status?: string;
}