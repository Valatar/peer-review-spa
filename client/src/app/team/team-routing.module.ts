import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShowTeamComponent } from './show-team/show-team.component';
import { DetailsTeamComponent } from './details-team/details-team.component';
import { TeamResolverService } from './services/team-resolver.service';
import { InvitationResolverService } from './services/invitation-resolver.service';
import { TeamWorkItemsResolverService } from './services/team-reviews-resolver.service';
import { TeamReviewsResolverService } from './services/team-work-items-resolver.service';
import { SingleTeamResolverService } from './services/single-team-resolver.service';
import { UserResolverService } from '../core/services/user-resolver.service';

const routes: Routes = [
  {path: '', component: ShowTeamComponent, pathMatch: 'full', resolve: {teams: TeamResolverService, invitations: InvitationResolverService}},
  {path: ':id', component: DetailsTeamComponent, resolve: { team: SingleTeamResolverService, workItems: TeamWorkItemsResolverService, reviews: TeamReviewsResolverService, users: UserResolverService }},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamRoutingModule { }
