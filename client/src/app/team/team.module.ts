import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowTeamComponent } from './show-team/show-team.component';
import { TeamRoutingModule } from './team-routing.module';
import { SharedModule } from '../shared/shared.module';
import { TokenInterceptorService } from '../interceptors/token.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorInterceptor } from '../interceptors/error.interceptor';
import { SpinnerInterceptor } from '../interceptors/spinner.interceptor';
import { TeamResolverService } from './services/team-resolver.service';
import { InvitationResolverService } from './services/invitation-resolver.service';
import { CreateTeamDialogComponent } from './show-team/create-team-dialog.component';
import { DetailsTeamComponent } from './details-team/details-team.component';
import { TeamWorkItemsResolverService } from './services/team-reviews-resolver.service';
import { TeamReviewsResolverService } from './services/team-work-items-resolver.service';
import { SingleTeamResolverService } from './services/single-team-resolver.service';
import { UserResolverService } from '../core/services/user-resolver.service';
import { AddMemberDialogComponent } from './details-team/add-member/add-member.component';

@NgModule({
  declarations: [ShowTeamComponent, CreateTeamDialogComponent, DetailsTeamComponent, AddMemberDialogComponent],
  imports: [
    CommonModule,
    TeamRoutingModule,
    SharedModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpinnerInterceptor,
      multi: true
    },
    TeamResolverService,
    InvitationResolverService,
    TeamWorkItemsResolverService,
    TeamReviewsResolverService,
    SingleTeamResolverService,
    UserResolverService
  ],
  entryComponents: [
    CreateTeamDialogComponent,
    AddMemberDialogComponent
  ]
})
export class TeamModule { }
