import { Component, OnInit } from '@angular/core';
import { Team } from 'src/app/common/models/team';
import { ActivatedRoute, Router } from '@angular/router';
import { Invitation } from 'src/app/common/models/invitation';
import { MatDialog } from '@angular/material';
import { CreateTeamDialogComponent } from './create-team-dialog.component';
import { TeamService } from 'src/app/core/services/team.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';

export interface CreateTeamData {
  name: string;
}

@Component({
  selector: 'app-show-team',
  templateUrl: './show-team.component.html',
  styleUrls: ['./show-team.component.sass']
})
export class ShowTeamComponent implements OnInit {

  teams: Team[] = [];
  invitations: Invitation[] = [];
  createTeamName: string = '';

  constructor(
    private readonly notificator: NotificatorService,
    private readonly route: ActivatedRoute,
    private readonly teamService: TeamService,
    public dialog: MatDialog,
    private readonly router: Router,
  ) { }

  ngOnInit(): void {
    this.getAllTeams();
  }

  getAllTeams(): void {
    this.route.data.subscribe(
      (data) => {
        this.teams = data.teams;
        this.invitations = data.invitations;
      }
    );
  }

  showTeam(id: string): void {
    this.router.navigate([`/team/${id}`]);
  }

  saveTeam(name: string): void {
    this.teamService.createTeam(name.trim())
      .subscribe(
        (result) => {
          this.teams.push(result);
          this.updateInvitationsList();
          this.notificator.success('Team has been created!')
        },
        (error) => {
          this.notificator.error(error)
        }
      )
  }

  createTeam(): void {
    const dialogRef = this.dialog.open(CreateTeamDialogComponent, {
      width: '360px',
      data: {name: this.createTeamName}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.createTeamName = result;
        this.saveTeam(result);
      }
    });
  }

  updateInvitation(id: string, answer: boolean): void {
    const status = answer ? 'accepted' : 'rejected';
    this.teamService.updateInvitation(id, status)
      .subscribe(
        (invitation) => {
          const index = this.invitations.findIndex(el => el.id === invitation.id);
          
          this.updateTeamsList();
          this.invitations.splice(index, 1, invitation);
          this.notificator.success(`The invitation was successfully ${answer ? 'accepted' : 'rejected'}!`);
        },
        (error) => {
          this.notificator.error(error);
        }
      )
  }

  updateTeamsList():void {
    this.teamService.getTeams()
      .subscribe(
        (res) => {
          this.teams = res;
        }
      );
  }

  updateInvitationsList(): void {
    this.teamService.getInvitations()
      .subscribe(
        (res) => {
          this.invitations = res;
        }
      );
  }
}

