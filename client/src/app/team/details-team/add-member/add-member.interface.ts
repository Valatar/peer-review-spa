import { User } from 'src/app/common/models/user';

export interface AddMemberData {
  allUsers: User[];
  teamId: string;
}
