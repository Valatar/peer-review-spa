import { Component, OnInit } from '@angular/core';
import { WorkItem } from 'src/app/common/models/work-item-view';
import { Team } from 'src/app/common/models/team';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/common/models/user';
import { MatDialog } from '@angular/material';
import { AddMemberDialogComponent } from './add-member/add-member.component';

@Component({
  selector: 'app-details-team',
  templateUrl: './details-team.component.html',
  styleUrls: ['./details-team.component.sass']
})
export class DetailsTeamComponent implements OnInit {

  workItems: WorkItem[] = [];
  reviews: WorkItem[] = [];
  team: Team;
  members: User[];
  users: User[] = [];

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.getInfo();
  }

  getInfo() {
    this.route.data.subscribe(
      (data) => {
        this.team = data.team;
        this.workItems = data.workItems;
        this.reviews = data.reviews;
        this.users = data.users
        this.filterMembers();
      }
    )
  }

  filterMembers() {
    const invitations = this.team.invitations;
    const allInvitedUsers = invitations.map(invitations => {
      return invitations.user;
    });
    const usersAcceptedInvitation = invitations.filter(invitation => {
      return invitation.status === 'accepted'
    }).map( invitation => {
      return invitation.user;
    });

    this.users = this.users.filter(user => {
      if(allInvitedUsers.findIndex(invitedUser => invitedUser.id === user.id) >= 0) {
        return false;
      }

      return true;
    });
    this.members = usersAcceptedInvitation;
  }

  addMemberDialog(): void {
    this.dialog.open(AddMemberDialogComponent, {
      width: '360px',
      height: '500px',
      data: {allUsers: this.users,teamId: this.team.id}
    });
  }

  showWorkItem(id: string) {
    this.router.navigate([`/work-item/${id}`]);
  }
}
