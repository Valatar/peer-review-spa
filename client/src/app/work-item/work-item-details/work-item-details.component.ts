import { WorkItem } from './../../common/models/work-item-view';
import { User } from './../../common/models/user';
import { AuthService } from './../../core/services/auth.service';
import { Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommentsService } from 'src/app/core/services/comments.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { WorkItemService } from 'src/app/core/services/work-item.service';

@Component({
  selector: 'app-work-item-details',
  templateUrl: './work-item-details.component.html',
  styleUrls: ['./work-item-details.component.sass']
})
export class WorkItemDetailsComponent implements OnInit {

  private userSubscription: Subscription;
  public loggedUser: User;
  public routeParamsSubscription: Subscription;
  public workItem: WorkItem;
  public assignee: string;
  public toggleEdit: boolean = false;
  public username: string = '';
  public newComment: string = '';
  public reviewers: User[] = [];
  public comments = [];
  public reviewer: boolean = false;
  public reviewersStr: string;

  constructor(
    private readonly authService: AuthService,
    private readonly commentService: CommentsService,
    private readonly workItemService: WorkItemService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly notificator: NotificatorService
  ) { }

  ngOnInit() {
    this.userSubscription = this.authService.currentUser$.subscribe(
      (res) => {
        this.loggedUser = res
        this.username = res.username;
      }
    );
    
    this.getWorkItem();
  }

  private getWorkItem(): void {
    this.route.data.subscribe(
      (data) => {
        console.log(data);
        this.workItem = data.workItem
        this.comments = data.workItem.comments;
        this.checkIfReviewerAndExtractReviewers();
        this.reviewersToStr();
      }
    )
  }

  reviewersToStr() {

    const strUsers = this.reviewers.map( user => {
      return user.username;
    });

    this.reviewersStr = strUsers.join(', ');
  }

  checkIfReviewerAndExtractReviewers(): void {
      this.workItem.review.forEach(review => {
        const user = review.__reviewer__;

        this.reviewers.push(user);

        if(user.username === this.username) {
          this.reviewer = true;
        }
      });
  }

  review(workItemId, status): void {

    this.workItemService.reviewWorkItem(status, workItemId)
      .subscribe(
        (res) => {
          this.workItem = res;
          this.notificator.success('The review was successfull!');
        },
        (error) => {
          this.notificator.error(error.error.error)
        }
      );
  }

  addComment(id: string): void {
    this.commentService.createComment(id, { content: this.newComment })
      .subscribe(
        (comment) => {
          this.comments.unshift(comment);
          this.notificator.success('New comment was created!');
        },
        (error) => {
          this.notificator.error(error.error.error)
        }
      )
  }

  editButton() {
    this.toggleEdit = !this.toggleEdit;
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }

  backToDashboard() {
    this.router.navigate(['/dashboard'])
  }

}
