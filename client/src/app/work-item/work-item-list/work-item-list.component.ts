import { CreateWorkItem } from './../../common/models/create-work-item';
import { WorkItemService } from './../../core/services/work-item.service';
import { Component, OnInit, Input } from '@angular/core';
import { WorkItem } from '../../common/models/work-item-view';
import { Subscription } from 'rxjs';
import { User } from '../../common/models/user';
import { NotificatorService } from '../../core/services/notificator.service';
import { ItemTag } from '../../common/models/item-tag';

@Component({
  selector: 'app-work-item-list',
  templateUrl: './work-item-list.component.html',
  styleUrls: ['./work-item-list.component.sass']
})
export class WorkItemListComponent implements OnInit{

  @Input() workItems: WorkItem[];
  public subscription: Subscription;
  public userSubscription: Subscription;
  public workItemTitle: string;
  public workItemDescription: string;
  public cancelCreation: boolean = true;
  @Input() allUsers: User[];
  public filteredUsers: User[];
  public selectedUsers: string[] = [];
  public tags: string[] = ItemTag;
  public chosenTag: string;

  constructor(
    private readonly workItemService: WorkItemService,
    private readonly notificator: NotificatorService,
  ) { }

  ngOnInit(): void {
    this.filterChange('');
  }

  createWorkItem() {

    const newItem: CreateWorkItem = {
      title: this.workItemTitle,
      description: this.workItemDescription,
      review: this.selectedUsers,
      tag: this.chosenTag
    }

    this.workItemService.createWorkItem(newItem).subscribe((res: any) =>{
      this.workItems.push(res);
      this.notificator.success('Work item was created');
    });

    this.cancelCreation = true;
    this.selectedUsers = [];
    this.workItemTitle = '';
    this. workItemDescription = '';
  }

  toggleButton() {
    this.cancelCreation = !this.cancelCreation;
  }

  toggleUser(id) {
    if (this.selectedUsers.includes(id)) {
      this.selectedUsers.splice(this.selectedUsers.indexOf(id), 1);
    } else {
      this.selectedUsers.push(id);
    }
  }

  filterChange(search: string): void {

    this.filteredUsers = this.allUsers.filter(user => {
      return user.username.match(search);
    });
  }

  chooseTag(tag) {
    this.chosenTag = tag;
  }
}

