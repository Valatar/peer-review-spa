import { AuthGuard } from './../auth/auth.guard';
import { WorkItemDetailsComponent } from './work-item-details/work-item-details.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { SingleWorkItemResolver } from './services/single-work-item-resolver.service';

export const routes: Routes = [
    { path: ':id', component: WorkItemDetailsComponent, canActivate: [AuthGuard], resolve: {workItem: SingleWorkItemResolver} } // to add resolver
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class WorkItemRoutingModule {}