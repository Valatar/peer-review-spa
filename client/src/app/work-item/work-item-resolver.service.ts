import { WorkItemService } from './../core/services/work-item.service';
import { WorkItem } from './../common/models/work-item-view';
import { Injectable } from "@angular/core";
import { NotificatorService } from '../core/services/notificator.service';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class WorkItemResolverService implements Resolve<WorkItem[]> {
    
    constructor(
        private readonly workItemService: WorkItemService,
        private readonly notificator: NotificatorService,
    ) {}

    public resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ) {
        return this.workItemService.allWorkItems()
        .pipe(catchError(
            res => {
                this.notificator.error(res.error.error);
                return of([]);
            }
        ))
    }
}