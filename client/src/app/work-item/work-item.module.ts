import { UserResolverService } from './../core/services/user-resolver.service';
import { WorkItemResolverService } from './work-item-resolver.service';
import { WorkItemRoutingModule } from './work-item-routing.module';
import { FormsModule } from '@angular/forms';
import { NgModule } from "@angular/core";
import { WorkItemDetailsComponent } from './work-item-details/work-item-details.component';
import { SharedModule } from '../shared/shared.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { SpinnerInterceptor } from '../interceptors/spinner.interceptor';
import { TokenInterceptorService } from '../interceptors/token.interceptor';
import { ErrorInterceptor } from '../interceptors/error.interceptor';
import { SingleWorkItemResolver } from './services/single-work-item-resolver.service';
import { CommentComponent } from './work-item-details/comment/comment.component';

@NgModule({
    declarations: [
        WorkItemDetailsComponent,
        CommentComponent
    ],
    imports: [SharedModule, FormsModule, WorkItemRoutingModule],
    providers: [
        WorkItemResolverService,
        UserResolverService,
        SingleWorkItemResolver,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptorService,
            multi: true,
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorInterceptor,
            multi: true,
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: SpinnerInterceptor,
            multi: true
        }
    ]
})
export class WorkItemModule {}