import { AuthService } from './../../core/services/auth.service';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit, OnDestroy {
  isLogged: boolean;
  private subscription: Subscription;

  constructor(private readonly authService: AuthService) { }

  ngOnInit() {
    this.isLogged = false;
    this.subscription = this.authService.isAuthenticated$.subscribe(
      (username) => {
        if (username === null) {
          this.isLogged = false;
        } else {
          this.isLogged = true;
        }
      },
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
