import { UserLogin } from './../../common/models/user-login';
import { AuthService } from './../../core/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { NotificatorService } from '../../core/services/notificator.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  hide = true;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  login(user: UserLogin) {
    this.authService.login(user).subscribe(
      res => {
        this.notificator.success(`${res.user.username} successfully logged in`);
        this.router.navigate(['/dashboard']);
      },
      (err) => {
        this.notificator.error(err.error.error)
      },
    );
  }
}
