import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})

export class CommentsService {
    constructor(
        private readonly http: HttpClient
    ) {}

    public createComment(workItemId, content) {
        return this.http.post(`http://localhost:3000/api/work-item/${workItemId}/comment`, content);
    }

    public updateComment( commentId, content) {
        return this.http.put(`http://localhost:3000/api/work-item/comment/${commentId}`, content);
    }

    public deleteComment( commentId) {
        return this.http.delete(`http://localhost:3000/api/work-item/comment/${commentId}`);
    }
}