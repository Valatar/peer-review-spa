import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../../common/models/user';
import { StorageService } from './storage.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { UserLogin } from '../../common/models/user-login';
import { tap } from 'rxjs/operators';
import { UserRegister } from '../../common/models/user-register';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root',
})

export class AuthService {

    private isAuthenticatedSubject$: BehaviorSubject<string | null> = new BehaviorSubject(this.username);
    private currentUserSubject$: BehaviorSubject<User> = new BehaviorSubject({} as User);

    constructor(
        private readonly http: HttpClient,
        private readonly storage: StorageService,
        private readonly jwtHepler: JwtHelperService,
        private readonly router: Router,
    ) {
        if (!this.username) {
            this.router.navigate(['/home']);
        } else if (this.isTokenExpired) {
            this.isAuthenticatedSubject$.next(null);
            this.router.navigate(['/login']);
        }
        else {
            this.http.get<User>(`http://localhost:3000/api/user/${this.username}`).subscribe(
                (res: any) => {
                    this.isAuthenticatedSubject$.next(res.username);
                    this.currentUserSubject$.next(res);
                }
            );
        }
    }

    public get isAuthenticated$(): Observable<string> {
        return this.isAuthenticatedSubject$.asObservable();
    }

    public get currentUser$(): Observable<User> {
        return this.currentUserSubject$.asObservable();
    }

    private get username(): string | null {
        const token = this.storage.get('token');
        if (token) {
            return this.jwtHepler.decodeToken(token).username;
        }
        return null;
    }

    private get isTokenExpired(): boolean {
        const token = this.storage.get('token');

        if (token) {
            return this.jwtHepler.isTokenExpired();
        }
        
        return false;
    }

    public login(user: UserLogin): Observable<any> {
        return this.http.post('http://localhost:3000/api/login', user).pipe(
            tap((res: any) => {
                this.isAuthenticatedSubject$.next(res.user.username);
                this.currentUserSubject$.next(res.user);
                this.storage.set('token', res.token);
            })
        );
    }

    public register(newUser: UserRegister): Observable<User> {
        return this.http.post<User>('http://localhost:3000/api/register', newUser);
    }

    public logout(): Observable<User> {
        const token = this.storage.get('token');
        return this.http.post('http://localhost:3000/api/logout', token).pipe(
            tap((res: User) => {
                this.isAuthenticatedSubject$.next(null);
                this.currentUserSubject$.next({} as User);
                this.storage.remove('token');
            })
        );
    }
}
