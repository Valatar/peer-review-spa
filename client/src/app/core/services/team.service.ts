import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { Team } from 'src/app/common/models/team';
import { Invitation } from 'src/app/common/models/invitation';

@Injectable({
    providedIn: 'root',
})
export class TeamService {
  constructor(
      private readonly http: HttpClient
  ) {}

  public getTeams(): Observable<Team[]>{
    return this.http.get<Team[]>('http://localhost:3000/api/team/all');
  }

  public getInvitations(): Observable<Invitation[]> {
    return this.http.get<Invitation[]>('http://localhost:3000/api/team/invitations');
  }

  public getSingleTeam(id: string): Observable<Team>{
    return this.http.get<Team>(`http://localhost:3000/api/team/${id}`);
  }

  public getSingleTeamWorkItems(id: string): Observable<any>{
    return this.http.get<any>(`http://localhost:3000/api/team/${id}/workItems`);
  }

  public getSingleTeamReviews(id: string): Observable<any> {
    return this.http.get<any>(`http://localhost:3000/api/team/${id}/reviews`);
  }

  public createTeam(name: string): Observable<Team> {
    return this.http.post<{name: string}>(`http://localhost:3000/api/team`, { name });
  }

  public inviteUser(idTeam: string, username: string): Observable<any>{
    return this.http.post<any>(`http://localhost:3000/api/team/${idTeam}/invite?username=${username}`,{})
  }

  public updateInvitation(id: string, status: string): Observable<Invitation> {
    return this.http.put<Invitation>(`http://localhost:3000/api/team/invitation/${id}`, {status})
  }
}