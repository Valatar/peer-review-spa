import { UpdateWorkItem } from './../../common/models/update-work-item';
import { CreateWorkItem } from './../../common/models/create-work-item';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { WorkItem } from '../../common/models/work-item-view';

@Injectable({
    providedIn: 'root'
})

export class WorkItemService {
    constructor(
        private readonly http: HttpClient
    ) {}

    public allWorkItems(): Observable<WorkItem[]> {
        return this.http.get<WorkItem[]>('http://localhost:3000/api/work-item');
    }

    public getOwnWorkItems(): Observable<WorkItem[]> {
        return this.http.get<WorkItem[]>('http://localhost:3000/api/work-item/own')
    }

    public getOwnReviewingWorkItems(): Observable<WorkItem[]> {
        return this.http.get<WorkItem[]>('http://localhost:3000/api/work-item/own/reviews')
    }

    public getWorkItemById(id: string): Observable<WorkItem> {
        return this.http.get<WorkItem>(`http://localhost:3000/api/work-item/${id}`);
    }

    public createWorkItem(newItem: CreateWorkItem): Observable<CreateWorkItem> {
        return this.http.post<CreateWorkItem>('http://localhost:3000/api/work-item', newItem);
    }

    public updateWorkItem(updateItem: UpdateWorkItem, id: string): Observable<WorkItem> {
        return this.http.put<WorkItem>(`http://localhost:3000/api/work-item/${id}`, updateItem);
    } 

    public reviewWorkItem(status: string, id: string): Observable<WorkItem> {
        return this.http.put<WorkItem>(`http://localhost:3000/api/work-item/review/${id}`, {status})
    }
}