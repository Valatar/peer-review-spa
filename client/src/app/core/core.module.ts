import { WorkItemService } from './services/work-item.service';
import { AuthService } from './services/auth.service';
import { NgModule, SkipSelf, Optional } from "@angular/core";
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { StorageService } from './services/storage.service';
import { NotificatorService } from './services/notificator.service';
import { TeamService } from './services/team.service';

@NgModule({
    imports: [
        ToastrModule.forRoot({
            positionClass: 'toast-bottom-center',
            preventDuplicates: true,
        }),
        HttpClientModule
    ],
    providers: [
        AuthService,
        StorageService,
        NotificatorService,
        WorkItemService,
        TeamService
    ]
})
export class CoreModule {
    constructor(@Optional() @SkipSelf() parent: CoreModule) {
        if (parent) {
            throw new Error('Core module is already provided.');
        }
    }
}