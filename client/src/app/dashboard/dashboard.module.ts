import { UserResolverService } from './../core/services/user-resolver.service';
import { WorkItemListComponent } from './../work-item/work-item-list/work-item-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dahsboard-routing.module';
import { SharedModule } from '../shared/shared.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptorService } from '../interceptors/token.interceptor';
import { ErrorInterceptor } from '../interceptors/error.interceptor';
import { SpinnerInterceptor } from '../interceptors/spinner.interceptor';
import { DashboardComponent } from './dashboard.component';
import { OwnWorkItemsResolverService } from './services/own-work-items-resolver.service';
import { ReviewingWorkItemsResolverService } from './services/reviewing-work-items-resolver.service';

@NgModule({
  declarations: [DashboardComponent, WorkItemListComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpinnerInterceptor,
      multi: true
    },
    OwnWorkItemsResolverService,
    ReviewingWorkItemsResolverService,
    UserResolverService
  ],
})
export class DashboardModule { }
