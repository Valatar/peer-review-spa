import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificatorService } from '../core/services/notificator.service';
import { AuthService } from '../core/services/auth.service';
import { WorkItemService } from '../core/services/work-item.service';
import { Subscription } from 'rxjs';
import { WorkItem } from '../common/models/work-item-view';
import { User } from '../common/models/user';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

  username = '';
  isAdmin = false;
  workItems: WorkItem[] = [];
  reviews: WorkItem[] = [];
  allUsers: User[]
  loggedUser: User;
  userSubscription: Subscription;

  private subscription: Subscription;
  private roleSubscription: Subscription;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly auth: AuthService,
  ) { }

  ngOnInit() {
    this.getOwnWorkItemsAndReviews();

    this.userSubscription = this.auth.currentUser$.subscribe(
      (res) => {
        this.loggedUser = res;
      },
      )
    
      //workaround to remove my own username
    this.allUsers.forEach(user => {
      if (user.id === this.loggedUser.id) {
        this.allUsers.splice(this.allUsers.indexOf(user), 1)
      } 
    });
  }

  private getOwnWorkItemsAndReviews() {
    this.route.data.subscribe(
      (data) => {
        this.workItems = data.workItems;
        this.reviews = data.reviews;
        this.allUsers = data.users
      }
    );
  }

  showWorkItem(id: string) {
    this.router.navigate([`/work-item/${id}`]);
  }
}
